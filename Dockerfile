FROM alpine:3 AS builder

ARG KUBE_VERSION=1.22.2
ARG HELM_VERSION=3.7.0
ENV HELM_PLUGINS /helm-plugins
ENV GOBIN=/usr/local/bin

RUN apk add --no-cache ca-certificates bash git openssh curl gettext
RUN wget -q https://storage.googleapis.com/kubernetes-release/release/v${KUBE_VERSION}/bin/linux/amd64/kubectl -O /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/kubectl
RUN wget -q https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm
RUN chmod +x /usr/local/bin/helm
RUN mkdir -v /helm-plugins
RUN wget -q https://github.com/mozilla/sops/releases/download/v3.7.1/sops-v3.7.1.linux -O /usr/local/bin/sops
RUN chmod +x /usr/local/bin/sops
RUN helm plugin install https://github.com/databus23/helm-diff
RUN helm plugin install https://github.com/jkroepke/helm-secrets
RUN wget -q https://github.com/roboll/helmfile/releases/download/v0.140.1/helmfile_linux_amd64 -O /usr/local/bin/helmfile \
    && chmod +x /usr/local/bin/helmfile
RUN chmod g+rwx /root \
    && mkdir /config \
    && chmod g+rwx /config

FROM alpine:3
ENV HELM_PLUGINS /helm-plugins
RUN wget -O /doppler.apk https://github.com/DopplerHQ/cli/releases/download/3.34.0/doppler_3.34.0_linux_amd64.apk
RUN apk add --no-cache ca-certificates bash git openssh curl gettext jq bind-tools
RUN apk add --allow-untrusted /doppler.apk && rm /doppler.apk
COPY --from=builder /usr/local/bin/kubectl /usr/local/bin/kubectl
COPY --from=builder /usr/local/bin/helm /usr/local/bin/helm
COPY --from=builder /usr/local/bin/helmfile /usr/local/bin/helmfile
COPY --from=builder /helm-plugins /helm-plugins
COPY --from=builder /usr/local/bin/sops /usr/local/bin/sops
WORKDIR /config
CMD bash

